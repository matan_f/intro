<?php
    class html {
        protected $title="EX3";
        protected $body="EX3";
        public function view(){
        echo"<html>
        <head>
        <title>$this->title</title>
        </head>
        <body>$this->body</body>
        </html>";
        }

        function __construct($title="",$body=""){
            if ($title!="")
            {
                $this->title=$title;   
            }
            if ($body!="")
            {
                $this->body=$body;
            }
        }
    }
    class coloredMessage extends html{
        protected $color='green'; 
        public function __set($property,$value){
            if($property=='color'){
                $colors=array('red','yellow','green','black','blue');
                if(in_array($value, $colors)){
                    $this->color=$value;
                }
                else{
                  die( "Error: this color is not available");  
                }
            }
        }
        public function show(){
            echo "<html>
            <head>
            <title>$this->title</title>
            </head>
            <body><p style = 'color: $this->color'> $this->body </p></body>
            </html>";
    }
}

class fontSize extends coloredMessage{
    protected $fontSize=18;
    public function __set($property,$value){
        if ($property=='color'){
                parent::__set($property,$value);}
        elseif($property=='fontSize'){
            if(filter_var($value,FILTER_VALIDATE_INT) && $value<=24 && $value>=10 ){
                $this->fontSize=$value;
            }
            else{ 
                die( "Error: this font size is not available");  
            }
        }

    }
    public function show(){
        echo "<html>
        <head>
        <title>$this->title</title>
        </head>
        <body><p style = 'color:$this->color; font-size:$this->fontSize' > $this->body </p></body>
</html>"; 
}
}

?>