<?php
    include "ex4db.php";
    include "books.php";
?>

<html>
    <head>
        <title>Ex4</title>
    </head>
    <body>
        <p>
            <?php
                $db = new DB4 ('localhost', 'intro', 'root', '');   
                $dbc = $db->connect();
                $query = new QueryEx4($dbc);
                $q = "SELECT books.title, users.name FROM books, users WHERE books.user_id=users.id";
                $result = $query ->query($q);
                echo '<br>';

                   
                if($result->num_rows > 0){
                    echo '<table>';
                    echo '<tr><th>Book Title</th><th>User Name</th></tr>';
                    while($row = $result->fetch_assoc()){
                        echo '<tr>';
                        echo '<td>'.$row['title'].'</td><td>'.$row['name'].'</td>';
                        echo '</tr>';
                    }


                    echo '</table>';

                }else{
                    echo "Sorry no results";
                }    
    


            ?>
        </p>
    </body>
</html>