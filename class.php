<?php
    class Message {
        protected $text="A simple message";
        public static $count=0;
        public function show(){
            echo "<p> $this->text </p>";

        }
        function __construct($text=""){
            ++self::$count;
            if ($text!= "")
            {
                $this->text=$text;
            }
        }
    }

    class redMessage extends Message {
        public function show(){
            echo "<p style = 'color:red'> $this->text </p>";
        }
    }

    class coloredMessage extends Message{
        protected $color;

        public function __set($property,$value){
            if($property=='color'){
                $colors=array('red','yellow','green','black');
                if(in_array($value, $colors)){
                    $this->color=$value;
                }
            }

        }
        public function show(){
            echo "<p style = 'color: $this->color'> $this->text </p>";
    }
}
    function showObject($object){
        $object->show();

    }
    //comment
    //last comment
    

?>